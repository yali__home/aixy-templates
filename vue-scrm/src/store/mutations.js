export default {
    // 切换 toast 状态
    toggleToast(state, t){
        if(t){
            state.showToast = true;
            state.toastTip = t;
        }else{
            state.showToast = false;
        }
    },
    // 切换 loading 状态 
    toggleLoading(state, payload){
        state.loading = !state.loading;
        console.log(`Global loading ${state.loading?'show':'hide'}`);
    },
    // 切换 popup 显示状态
    togglePop(state, payload){
        state.showPopup = !state.showPopup;
        console.log(`Global popup ${state.showPopup?'show':'hide'}`);
    },
    // 设置 popup
    setPopConf(state, conf){
        console.log('set popConf triggered.');
        state.popConf = conf;
    },
    // 设置 popup 内容
    setPopCon(state, con){
        console.log('set popCon triggered.');
        state.popCon = con;
    },
    setRedirectUrl(state, payload){
        state.redirectUrl = payload;
    }
}