import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import cart from './modules/cart';
import user from './modules/user';
Vue.use(Vuex);

let store = new Vuex.Store({
    state(){
        return {
            showLoading: false,  //全局 loading
            showToast: false, //全局toast
            toastTip:  '',
            showPopup: false,
            popConf: {},
            popCon: '',
            redirectUrl: '', //重定向的url
            hasMenu: false
        }
    },
    mutations,
    modules: {
        user,
        cart
    }
})
export default store;