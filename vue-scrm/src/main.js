import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router';
import store from './store';
import vform from 'aixy-vue2-form';
// import list from 'aixy-vue2-list';
import toast from 'aixy-vue2-toast';
import popup from 'aixy-vue2-popup';
import loading from 'aixy-vue2-loading';
import App from './App.vue';
import bus from 'aixy-vue2-bus';
import './core/directive';

// 入口页面，各种插件使用的地方：vue-router、vuex
// rem
Vue.use(VueRouter);

// 注册全局组件
Vue.component('vform', vform);
// Vue.component('list', list);
Vue.component('toast', toast);
Vue.component('popup', popup);
Vue.component('loading', loading);

// 全局事件总线
Vue.prototype.$bus = bus;

// 挂载到 #app
let app = new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: {
		App
	}
});
