let  theme = require('./theme');
let { version } = require('./package.json');
let COLOR = process.env.THEME_COLOR || '';

module.exports.profile = 'vue';
module.exports.init = function(Config){
    return class MyConfig extends Config{
         constructor(){
             super();
            //  默认的皮肤配置为蓝色
             this.config.styleLoaderOptions = {
                 less: {
                     lessOptions: {
                         modifyVars: theme[COLOR]||{}
                     }
                 }
             }
             
            //  如果系统变量中有 THEME_COLOR 值，多皮肤目录随着产出
            if(COLOR && theme[COLOR]){
                this.config.assertPath = `theme_${COLOR}/${version}`;
                this.config.styleLoaderOptions.less.lessOptions.modifyVars = theme[COLOR];
            }

            // 启用自己的 server
            this.config.devServer.useBuildInServer = true;
            
         }
         onInit(){
            
         }
         onConfig(config){
             
         }
    }
}