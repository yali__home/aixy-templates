exports.profile = 'microapp';

let themeData = require('./theme');
// 可尝试修改这里的值，再看效果
let COLOR = process.env.THEME_COLOR || 'default';

exports.init = function(Config){
    return  class MyConfig  extends Config{
        constructor(){
            super();
            this.setConfig({
                indexPage: 'pages/form/form',
                lessOptions: {
                    modifyVars: themeData[COLOR]
                },
                themeData,
                subPackages: ['other'],
                enableTheme: true,
                themes: Object.keys(themeData),
                assetsDir: ['images/**/*'],
                // 全局组件的使用
                globalComponents:{
                    enable: false,
                    components: {
                        'loading': '~loading'
                    },
                    injection: function(file){
                        return '<loading></loading>'; 
                    }
                }
            });
        }
    }
}