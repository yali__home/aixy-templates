import { setTitle } from './sdk';

function Build(pageConf){
    let conf = {
        $setTitle: setTitle
    };
    conf = Object.assign({}, pageConf, conf);
    
    Page(conf);
}

export default Build;