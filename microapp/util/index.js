export function getType(o){
    return Object.prototype.toString.call(o).slice(8,-1);
}