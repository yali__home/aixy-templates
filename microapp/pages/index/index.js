import { getType } from '@/util/index.js';
import Reg from '@/core/page';
let prefix = process.env.themePrefix;

let app = getApp();

Reg({
    components: {
        'toast': "~toast"
    },
    data: {
        show: false,
        tip: 'Hello world',
        __THEME__: ''  //无默认皮肤
    },
    onShow(){
        console.log('page show');
        console.log('我的数据类型是：');
        console.log(getType({}));
        console.log(app.globalData);
        this.$setTitle('首页');
    },
    onLoad(){
        console.log('page load');
    },
    onReady(){
        console.log('page ready');
    },
    onUnload(){
        console.log('page onUnload 页面在卸载');
    },
    showTip(){
        this.toggleTpast(true);
    },
    onHide(){
        console.log('page hide');
    },
    toggleTpast(bool){
        this.setData({
            show: bool
        });
    },
    toggleTheme(e){
        let { type } = e.currentTarget.dataset;
        this.setData({
            __THEME__: `${prefix}${type}`
        })
    },
    go(e){
        let { page, pack } = e.currentTarget.dataset;
        wx.navigateTo({
            url: `${pack?('/'+pack):''}/pages/${page}/${page}`,
            // events: {
            //     test: function(e){
            //         console.log('test 页面发数据给我了');
            //         console.log(e);
            //     }
            // },
            success: (res)=>{
                console.log('跳转成功的 res  是什么？');
                // console.log(res);
                // 主动向test 页面发数据
                console.log(res.eventChannel);
                // setTimeout(()=>{
                //     // 这里为啥会报错
                //     res.eventChannel.emit('hi', { msg: '英子开门，我是你爹' });
                // },1000);
            }
        })
    },
    onPageScroll(e){
        console.log('onPageScroll 页面滚动中：');
        console.log(e.scrollTop);
    }
    // onPullDownRefresh(e){
    //     console.log(e);
    //     console.log('onPullDownRefresh');
    // },
    // onReachBottom(e){
    //     console.log(e);
    //     console.log('onReachBottom');
    // }
})