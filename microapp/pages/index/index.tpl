<view class="P-index">
    <view class="t s">欢迎来到 aixy 微信小程序案例展示</view>

    <view class="btn">
        <!-- <button bindtap="showTip">请点我弹出 toast 组件</button> -->
    </view>

    <view class="t">这是一个 base64 嵌入的图标哦</view>
    <view class="base64"></view>

    <view class="btn theme">
        <button bindtap="toggleTheme" data-type="">默认皮肤</button>
        <button bindtap="toggleTheme" data-type="pink">粉色皮肤</button>
        <button bindtap="toggleTheme" data-type="blue">蓝色皮肤</button>
    </view>

    <view class="t">去其它页面看看吧</view>

    <view class="btn theme">
        <button bindtap="go" data-page="test" data-pack="other">test 页</button>
        <button bindtap="go" data-page="form">form 页</button>
        <button bindtap="go" data-page="list">list 页</button>
    </view>

    <view class="t s">本页面是没有配置 json 文件的，json 文件是 aixy 自动生成的，组件引用路径，也是 aixy 自动生成的。可以看一下 pages/index 的源码哦</view>

    <view class="t">
        <view class="cell">本地根目录 images 下图片</view>
        <view class="cell">远程图片(微信小程序开发工具查看 css)</view>
    </view>

    <view class="pic">
        <image src="/images/smile.jpg" width="100rpx" height="100rpx"></image>
        <view class="cell"></view>
    </view>

    <!-- <view>{{helper.add(10, 20)}}</view> -->

</view>
<wxs src="../../util/helper.wxs" module="helper" />