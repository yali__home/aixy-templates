import Reg from '@/core/page';
Reg({
    components: {
        form: '~form',
    },
    data: {
        orders: [
            {
                id: 1,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 12.5,
                status: 2,
            },
            {
                id: 2,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 22.5,
                status: 1,
            },
            {
                id: 3,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            },
            {
                id: 4,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            },
            {
                id: 5,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            },
            {
                id: 6,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            },
            {
                id: 7,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            },
            {
                id: 8,
                shop_name: "天猫旗舰店",
                name: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                desc: "儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁儿童牙刷U型牙刷可爱樱花粉[2-6]岁",
                price: 32.5,
                status: 0,
            }
        ],
        formConfig: [
            {
                key: 'name',
                required: true,
                type: 'input',
                inputType: 'text',
                title: '姓名',
                val: ''
            },
            {
                key: 'age',
                required: true,
                type: 'input',
                inputType: 'number',
                title: '年龄',
                val: ''
            },
            {
                key: 'sex',
                required: true,
                type: 'radio',
                title: '性别',
                options: [
                    {
                        val: 0,
                        text: '男'
                    },
                    {
                        val: 1,
                        text: '女'
                    }
                ],
                val: 0
            },
            {
                key: 'id',
                required: true,
                type: 'input',
                title: '身份证号',
                inputType: 'idcard',
                val: ''
            },
            {
                key: 'birth',
                required: true,
                // type: 'switch',
                title: '出生日期',
                val: ''
            },
            {
                key: 'hobby',
                type: 'checkbox',
                required: true,
                title: '爱好',
                options: [
                    {
                        val: 0,
                        text: '跑步'
                    },
                    {
                        val: 1,
                        text: '跳绳'
                    },
                    {
                        val: 2,
                        text: '游泳'
                    },
                    {
                        val: 3,
                        text: '滑雪'
                    }
                ],
                val: []
            },
            {
                key: 'turn',
                type: 'switch',
                required: true,
                title: '开启关注',
                val: true
            },
            {
                key: 'desc',
                required: true,
                type: 'textarea',
                title: '描述',
                val: '你们好'
            },
            {
                key: 'address',
                required: true,
                type: 'custom',
                title: '省市区',
                val: ''
            },
        ]
    },
    onShow(){
        this.$setTitle('表单页');
    }
});