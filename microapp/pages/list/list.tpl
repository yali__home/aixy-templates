<view class="P-list">
    <list generic:component="order-card" data="{{orders}}">
        <view slot="before" class="P-listHead">列表顶部是可以自定义内容的哦</view>
        <view slot="after" class="P-listHead">列表尾部也是可以自定义内容的哦</view>
    </list>
</view>