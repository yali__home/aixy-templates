<view class="M-toast" wx:if="{{show}}">
    <view class="mask">{{msg}}</view>
    <view class="con">{{text}}</view>
</view>