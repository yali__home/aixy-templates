Component({
    properties: {
        text: {
            type: String,
            value: ''
        },
        show: {
            type: Boolean,
            value: false
        }
    },
    data: {
        mes: ''
    },
    attached(){
        console.log('toast attached.');
        console.log(this);
        if(this.data.show){
            setTimeout(()=>{
                console.log('2s 后关闭');
                this.update(false);
            }, 2000);
        }
    },
    created(){
        console.log('组件实例初始化');
        console.log(this);
        // console.log(this.setData);
        this.setData({
            msg: 'good'
        })
    },
    detached(){
        console.log('组件退出页面节点树')
    },
    methods: {
        update(bool){
            this.setData({
                show: bool
            })
        }
    },
    observers: {
        show: function(nVal, oVal){
            if(nVal && nVal!=oVal){
                console.log('数据变化了：');
                console.log(nVal);
                setTimeout(()=>{
                    console.log('2s 后关闭');
                    this.update(false);
                }, 2000);
            }
        }
    }
});