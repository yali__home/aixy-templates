<scroll-view class="M-list" scroll-y="true" bindscrolltolower="toBottom">
    <!-- 列表头部有内容 -->
    <slot name="before"></slot>

    <!-- 列表本身 -->
    <view class="M-listWrap">
        <view class="M-listItem" wx:for="{{items}}" wx:key="id">
            <component item="{{item}}" index="{{index}}" ext="{{config.ext}}"></component>
        </view>
    </view>

    <!-- 自定义 empty: 没有内容的时候 -->
    <slot name="empty" wx:if="{{config.customEmpty}}"></slot>
    <!-- 默认 empty: 没有内容的时候 -->

    <view class="M-listEmpty" wx:if="{{(!items || !items.length) && !config.customEmpty}}">
        <view class="M-listEmptyIcon"></view>
        <view class="M-listEmptyText">{{config.emptyTip}}</view>
    </view>

    <!-- 内置默认 empty:  显示于列表尾部 -->
    <view class="M-listEmptyTip" wx:if="{{end}}">{{config.endTip||'没有更多内容'}}</view>

    <!-- 列表尾部 有内容 -->
    <slot name="after"></slot>
</scroll-view>