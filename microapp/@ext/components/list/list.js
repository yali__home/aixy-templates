const app = getApp();
Component({
    options: {
        multipleSlots: true
    },
    properties: {
        config: {
            type: Object,
            value: {
                req: {
                    url: '',
                    query: {}  //传给 url 的 query 参数，通过监听此参数变化来重新去拉取数据
                },
                pageSize: 10,  //每页数据条数
                emptyTip: '',  //默认数据为空时候的提示
                endTip: '',  //默认的数据到底的提示
                customEmpty: false,  //如果你想自定义 empty，启用此选项即可
                ext: {

                }
            }
        },
        data: {
            type: Array,
            value: []
        }
    },
    data: {
        end: false,
        items: []
    },
    attached() {
        // 当前数据页
        this.curPage = 1;
        // 总数据条数
        this.total = 0;
        this.lock = false;
        let config = this.data.config;
        if (config.req && config.req.url) {
            //发送请求去请求数据
            this.lock = true;
            this.loadData();
        }

        if (this.data.data && this.data.data.length) {
            this.setData({
                items: this.data.data
            });
        }
    },
    methods: {
        toBottom(e) {
            // 列表为非锁定状态，且数据库还有数据，可以去拉取数据
            let data = this.data;

            if (!this.lock && !data.end && data.config.req.url) {
                this.lock = true;
                // 页面增加 1
                this.curPage += 1;
                this.loadData();
            }
        },
        loadData(override) {
            //根据 curPage，拉取数据
            let { pageSize = 10, req } = this.data.config;
            //此处具体看不同项目如何封装 wx.request，替换成对应的使用方式即可
            app.request.get(req.url, {
                ent_id: app.g('ent_id'),
                limit: pageSize,
                page_no: this.curPage,
                ...req.query
            }).then(res => {
                // 列表数据请求成功
                if (res) {
                    if (res.items && res.items.length) {
                        let items = !override?this.data.items.concat(res.items):res.items;
                        this.setData({
                            items
                        });
                        this.total = items.length;
                    }else{
                        console.log('结尾了');
                        this.data.end = true;
                        this.setData({
                            end: true
                        })
                    }
                }
                this.lock = false;
            }, err => {
                //列表数据请求失败
                this.lock = false;
            });
        }
    },
    observers: {
        'config.req': function (nVal, oVal) {
            //监听请求对象，如果发生变化，则重新请求数据
            this.curPage = 1;
            this.lock = true;
            this.loadData(true);
        }
    }
})