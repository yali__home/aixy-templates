<view class="M-form" bindscrolltolower="toBottom">
    <!-- 输入框、radio、checkbox、获取手机 -->
    <view class="M-formCell" wx:for="{{config}}" wx:key="key">
        <view class="M-formCellLabel">
            <view class="required" wx:if="{{item.required}}">*</view>
            <view class="title">{{item.title}}</view>
        </view>
        <view class="M-formCellCtrl">
            <!-- 控件 -->
            <!-- 目前可处理光标，可处理输入完毕键盘自动收缩 -->
            <!-- 文字输入: type 同小程序 type -->
            <input wx:if="{{item.type=='input'}}" type="{{item.inputType||'text'}}" value="{{result[item.key]}}" data-key="{{item.key}}" bindinput="inputChange" />

            <!-- 大段文本输入，值的获取 -->
            <textarea wx:if="{{item.type=='textarea'}}" value="{{result[item.key]}}" bindinput="inputChange" data-key="{{item.key}}"></textarea>

            <!-- checkbox -->
            <checkbox-group wx:if="{{item.type=='checkbox'}}" bindchange="changeCheckbox" data-item="{{item}}">
                <checkbox wx:for="{{item.options}}" wx:for-item="check" wx:key="val" checked="{{result[item.key][check.val].checked}}" disabled="{{result[item.key][check.val].disabled}}" value="{{check.val}}">{{check.text}}</checkbox>
            </checkbox-group>

            <!-- radio -->
            <radio-group wx:if="{{item.type=='radio'}}" bindchange="changeRadio" data-key="{{item.key}}">
                <radio wx:for="{{item.options}}" wx:key="val" checked="{{result[item.key]==radio.val?true:false}}" disabled="{{result[item.key][radio.val].disabled}}" wx:for-item="radio" value="{{radio.val}}">{{radio.text}}</radio>
            </radio-group>

            <!-- switch -->
            <switch wx:if="{{item.type=='switch'}}" bindchange="changeSwitch" data-key="{{item.key}}" color="{{item.color}}" checked="{{result[item.key]}}"></switch>

            <!-- 省市区 -->

            <!-- 年月日 -->
            
            <!-- 时分秒 -->

            <!-- 自定义 slot -->
            <block wx:if="{{item.type=='custom'}}">
                <slot name="{{item.key}}"></slot>
            </block>
        </view>
    </view>
</view>