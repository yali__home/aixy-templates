Component({
    options: {
        multipleSlots: true
    },
    properties: {
        config: {
            type: Array,
            value: []
        },
        data: {
            type: Array,
            value: []
        }
    },
    data: {
        result: [],
        checkboxMap: {}
    },
    attached(){
        this.getResult();
    },
    methods: {
        getResult(){
            let config = this.data.config;
            let result = {};
            let checkboxMap = {};
            config.forEach(ele=>{
                result[ele.key] = ele.val;
                // 给 checkbox 构建一个 map
                if(ele.type=='checkbox'){
                    checkboxMap[ele.key] = {};
                    ele.options.forEach(item=>{
                        checkboxMap[ele.key][item.val] = item;
                    })
                }
            });
            this.setData({
                result
            });
        },
        inputChange(e){
            let { key } = e.currentTarget.dataset;
            let { value } = e.detail;
            console.log('inputChange:');
            console.log(value);
            this.setData({
                [`result.${key}`]: value
            });
        },
        changeCheckbox(e){
            let { item } = e.currentTarget.dataset;
            let { value } = e.detail;
            console.log('changeCheckbox:');
            console.log(value);
            let res = {};
            item.options.forEach(ele=>{
                if(!res[ele.val]) res[ele.val] = {};
                res[ele.val].checked = (value.indexOf(String(ele.val))>-1)?true:false;
            });
            this.setData({
                [`result.${item.key}`]: res
            });
        },
        changeRadio(e){
            let { key } = e.currentTarget.dataset;
            let { value } = e.detail;
            this.setData({
                [`result.${key}`]: value
            });
        },
        /**
         * 构建 checkbox/radio 数据
         * @param {*} val 选中的值
         * @param {*} options 选项
         */
        buildCheckVal(val,  options){

        },
        changeSwitch(e){
            let { key } = e.currentTarget.dataset;
            let { value } = e.detail;
            console.log('changeSwitch:'+value);
            this.setData({
                [`result.${key}`]: value
            })
        }
    }
})