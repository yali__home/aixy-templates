<view class="M-cardOrder">
    <!-- @click="cardHandler" -->
    <view class="M-cardOrderHead">
        <span>{{ item.shop_name }}</span><i>{{ statusMap[item.status] }}</i>
    </view>
    <view class="M-cardOrderCon">
        <view class="pic">
        </view>
        <view class="con">
            <view class="t">{{ item.name }}</view>
            <view class="desc">{{ item.desc }}</view>
        </view>
        <view class="right">
            <span class="price">
                ￥{{ item.price }}
            </span>
        </view>
    </view>
</view>