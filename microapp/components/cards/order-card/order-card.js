Component({
    properties: {
        item: {
            type: Object,
            value: {}
        },
        index: Number,
        ext: Object
    },
    data: {
        statusMap: {
            0: "待付款",
            1: "待发货",
            2: "待收货",
            3: "已完成"
        }
    },
    attached() {
        console.log('order-card attached');
    },
    created() {
        console.log('order-card created');
    },
    detached() {
        console.log('order-card detached');
    },
    ready() {
        console.log('order-card ready');
    },
    moved() {
        console.log('order-card moved');
    },
    // add(e) {
    //     this.update();
    // },
    update: function () {
        // 更新 myValue
        this.setData({
            index: (this.data.index+1)
        })
    }
})