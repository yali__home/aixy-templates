import Reg from '@/core/page';
import { getType } from '@/util/index.js';
let prefix = process.env.themePrefix;
let app = getApp();
// 可否所有页面上来收打开都注册一个公共事件，公共数据一旦发生变化，就触发当前页面的，其余页面每次进来不需要触发
Reg({
    components: {
        'toast': "~toast",
        'order-card': "~cards/order-card"
    },
    data: {
        show: false,
        tip: 'Hello world',
        __THEME__: `${prefix}pink`,
        items: [
            { value: 'USA', name: '美国' },
            { value: 'CHN', name: '中国', checked: 'true' },
            { value: 'BRA', name: '巴西' },
            { value: 'JPN', name: '日本' },
            { value: 'ENG', name: '英国' },
            { value: 'FRA', name: '法国' }
        ],
        order: {
            id: 2,
            desc: '沙发挂号费市公检法地方都是三个地方三个地方是的'
        },
        show: true,
        name: '',
        index: 0
    },
    toggleShow(){
        this.setData({
            show: !this.data.show
        });
    },
    onShow() {
        console.log('test show');
        console.log('我的数据类型是：');
        console.log(getType({}));
        console.log(app.globalData);
    },
    onLoad() {
        console.log('test load');
        // let eventChannel = this.getOpenerEventChannel();
        // eventChannel.emit('test', {msg: '老爸，我到家了'});
        // eventChannel.on('hi', {msg: '知道了，一会儿就开'});
    },
    onReady() {
        console.log('test ready');
    },
    checkboxChange(e) {
        // e.detail.value checkbox 的值
        console.log('checkbox发生change事件，携带value值为：', e.detail.value);
        console.log(e);
    },
    changeRadio(e) {
        console.log('changeRadio:');
        console.log(e.detail.value);
    },
    onUnload() {
        console.log('test onUnload 页面在卸载');
    },
    showTip() {
        this.toggleTpast(true);
        setTimeout(() => {
            this.toggleTpast(false);
        }, 2000);
    },
    toggleTpast(bool) {
        this.setData({
            show: bool
        });
    },
    toggleTheme(e) {
        let { type } = e.currentTarget.dataset;
        this.setData({
            __THEME__: `${prefix}${type}`
        })
    }
})