<view class="P-index">
    <view>
        <input type="text" model:value="{{name}}" />{{name}}
    </view>
    <checkbox-group bindchange="checkboxChange">
        <label class="weui-cell weui-check__label" wx:for="{{items}}" wx:key="{{item.value}}">
          <view class="weui-cell__hd">
            <checkbox value="{{item.value}}" checked="{{item.checked}}"/>
          </view>
          <view class="weui-cell__bd">{{item.name}}</view>
        </label>
    </checkbox-group>

    <radio-group bindchange="changeRadio">
        <view wx:for="{{items}}" wx:key="{{item.value}}">
            <radio value="{{item.value}}" checked="{{item.checked}}" /></radio>
        </view>
    </radio-group>
    <view><button bindtap="toggleShow">切换组件状态</button></view>{{index}}
    <order-card wx:if="{{show}}" item="{{order}}" index="{{index}}"></order-card>
</view>