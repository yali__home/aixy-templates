#### 环境安装

请先全局安装  aixy

```
npm install  aixy -g

安装完毕后请再运行以下命令，确认一下是否安装成功
aixy -V
```

然后把项目中的 `@config/config.sandbox.js` 中 appId 换成你自己的，然后运行以下命令：
```
aixy dev --theme
```

然后在小程序开发人员工具中导入当前项目根目录下的 `dist/wechat/sandbox/project` 目录，小程序就算正式跑起来了

#### 小程序端能力

* **css 预编译(Less)**
&nbsp;
    原生小程序的 .wxss 和 .acss 文件，我们可以直接替换为 .less
&nbsp;

* **Less import 文件路径自动转换**
&nbsp;
    less 中以 import 方式引入项目根目录下的公共 less 文件可以写成：`@import '~/common/var.less'`，`~` 会被自动替换为项目根目录，也就是说你不需要管当前 less 文件相对公共 less 文件的位置，用 `~` 就对了  
&nbsp;

* **多皮肤**
&nbsp;
    1. 当前项目我定义了 3 种皮肤(默认(green)、pink、blue)，
    2. 当前项目 `override.config.js` 文件中的 `enableTheme` 已经开启，`themeData` 和 `themes` 配置已经都配置好了
    3. 在需要应用多皮肤的页面 less 文件中的需要应用多皮肤的节点的类名下放置 `/*__ROOT__*/` 这段文本标志，这个页面/组件的多皮肤编译就算开启了，如果你不想对某个页面或组件应用多皮肤，不加这个标志就行了，如果你看不懂我在说什么，全局搜索一下 `/*__ROOT__*/` 吧
    4. 运行了 `aixy dev --theme` 命令后，可以去 dist 目录下对比一下加了  `/*__ROOT__*/` 标志和没加的 less 文件产出的 wxss，加了 `/*__ROOT__*/` 标志的 less 文件产出到 dist 的 wxss 文件中会多出 `.__T-pink` 和 `.__T-blue` 前置类名，多皮肤就是通过这种类名优先级权重叠加的方式实现的
    5. 以上 4 点讲的是如何编译得到具有多种皮肤样式的 wxss 文件，那么样式文件有了，小程序如何决定使用哪一种皮肤呢？继续往下看
    6. 开启了多皮肤后，构建工具在编译的时候会给每个页面和组件的模板文件的**一级节点**都加上一个动态 class  `{{__THEME__}}` 变量，小程序内部就可以通过设置这个值(setData)来决定皮肤类型的，可以参考我在 `pages/index/index` 里面的写法
&nbsp;
* **页面引用的组件的路径自动识别生成(包括分包)**
&nbsp;
    我在 `pages/list/list` 页面的 js 中配置了：

    ```
    components: {
        list: '&list',
        'order-card': '~cards/order-card',
        'vform': 'vform'
    }
    ```
    然后 dist 目录中产出的 list.json 中自动生成了以下内容：
    ```
    {
        "navigationStyle": "default",
        "usingComponents": {
            // 使用 `&` 的话，构建工具会自动帮助计算当前页距离 components 的相对路径
            "list": "../../components/list/list",  
            // 使用 `~` 的话，路径会转换为绝对路径
            "order-card": "/components/cards/order-card/order-card"
            // npm 组件
            'vform': 'vform'
        }
    }
    ```
    为什么工具要扩展这个功能呢？想象一下，如果你的某个组件，在很多页面中都有应用到，然后某一天你突然改变了它的位置，难道你要去把使用它的文件都找出来，一个一个改路径吗？
&nbsp;
* **非页面/非组件 js 文件引用路径自动生成(包括分包)**
&nbsp;
    我在页面中引用了一个公共 js 文件：
    ```
    import Reg from '@/core/page';
    ```
    在产出文件中自动解析为了：
    ```
    import Reg from "../../core/page";
    ```
    道理和前面 Less import 引用是一样的
&nbsp;
* **图片自动转 base64 嵌入**
&nbsp;
    此功能是参照的 webpack 的 file-loader，在引用了某个小图标的 less 文件中，在引用图片的 url 后面加上 `?__inline` 参数，这张图就会自动被转换为 base64 了，可查看 `pages/index/index` 页面的示例
&nbsp;
* **本地以相对路径引入的图片 url 自动拼接图片云服务域名、带 hash**
&nbsp;
    首先，`useHash` 是默认开启的，然后，我在 `@config/config.sandbox.js` 中配置了 imageDomain，然后，我在 package.json 中配置了 `enterprise` 字段，然后构建工具就自动的把图片云服务域名加到了样式文件中的图片 url 上，可以参见 `pages/index/index` 页面最底部示例;
    编译完毕后，在  `dist/wechat/sandbox/public/${enterprise}` 目录下，你会看到所有这些自动带上 iageDomain 的图片，都被按照原路径提取到了这里，项目开发完毕，直接把这个文件，整个扔到图片云服务上即可
&nbsp;
* **多项目共享内容合并策略**
&nbsp;
    试想一下这种场景，你有 2 个以上的小程序，它们有非常多的页面(pages)、组件(components)功能有重叠，又有部分 页面和组件功能不重叠，你要怎么去管理他们呢？aixy 提供了一个 @ext 目录，你可以在这个目录下放置公共的 components、pages 或者其它内容，@ext 内部的内容最终在构建的时候都会自动从 @ext 中提出来，合并到项目根目录，当前的这个项目中，我已经把通用组件都放到了 @ext，可以编译一下，然后去 dist 目录查看编译效果   
&nbsp;
* **全局自动注入公共内容至页面 js 和 模板**
    某些组件是全局可用的，比如：loading；那么问题来了，我每个页面都引入一次？某天我修改了它的 properties，我要每个页面去改动吗？这个特性就是为了解决这个问题而生的，配置如下：
    ```
    // 在 override.config.js 中配置
    globalComponents:{
        enable: true,
        components: {
            'loading': '~loading'  //组件的路径处理方式和页面级别组件引入方式一样
        },
        //此函数执行结果会自动注入到所有页面模板中
        injection: function(file){ return '<loading show="{{showloading}}"></loading>' }
    }
    ```
    可以在 dist 目录中看到 app.json 中多出了这部分内容：
    ```
    "usingComponents": {
        "loading": "/components/loading/loading"
    }
    ```
    在 dist 目录下的所有页面模板中都多出了 injection 中返回的内容，可以自己尝试一下(可能会看到小程序开发人员工具控制台报错，因为我没有这个 loading 组件，你只要看到页面模板发生了变化就好了)
&nbsp;
* **环境变量注入模板/js**
&nbsp;
    此功能参考的是 webpack.definePlugin，模板中注入的话就通过如下方式：
    ```
    {# process.env.NODE_ENV #}
    ```
    模板中注入变量，借用的是 art-template 模板的能力
    js 中注入的话就直接使用 `let env = process.env.NODE_ENV` 然后直接使用变量 `env` 即可
&nbsp;
* **app.json 与 plugin.json 配置自动生成**
&nbsp;
    app.json 和 plugin.json 中的 pages 对象，工具直接自动生成了，不需要每次新增或者减少一个页面，都要手动去修改一下；
    不仅如此，分包的部分配置也是自动生成的；我在 override.config.js 中配置了 subPackages字段，也就是分包目录 other，然后，考虑到以往做过的一些复杂项目，在分包中还存在着插件引用，且插件还分 sandbox、production，对应的版本只要稍微管理不好，发到线上那可就出事了(作者血泪史)；在 @config 目录中相关的环境配置文件中可以为分包做更细粒度的配置，配置如下
    ```
    module.exports = {
        imageDomain: 'http://www.baidu.com/sandbox',
        appId: 'wxae265b8ed362a2b5',
        plugins: {
            "advert": {
                "version": "2.2.4",
                "provider": "wxc186862fc4774ff9"
            },
            "chooseLocation": {
                "version": "1.0.6",
                "provider": "wx76a9a06e5b4e693e"
            }
        },
        subPackages: {
            "other": {
                plugins: {
                    "booking": {
                        "version": "1.0.78",
                        "provider": "wx16d3c97c36870289"
                    },
                    "captcha": {
                        "version": "1.1.4",
                        "provider": "wxb7c8f9ea9ceb4663"
                    }
                }
            }
        }
    }
    ``` 
    编译产出的 app.json 如下：
    ```
    {
        "subPackages": [
            {
                "root": "other",
                "pages": [
                    "pages/test/test"
                ],
                "plugins": {
                    "booking": {
                        "version": "1.0.78",
                        "provider": "wx16d3c97c36870289"
                    },
                    "captcha": {
                        "version": "1.1.4",
                        "provider": "wxb7c8f9ea9ceb4663"
                    }
                }
            }
        ]
    }
    ```
    编译的时候，你只需要确定 .env 文件中的 NODE_ENV 配置是对的，NODE_ENV 对应的 @config 目录下的配置文件的配置是对的，就再也不需要担心其它问题了
    本项目给出的 plugins 配置编译后，小程序可能无法正常运行，最好换成你自己的 plugins 配置，再看
&nbsp;
* **多平台多环境产出**
&nbsp;
    就是通过修改 .env 文件中 `NODE_ENV` 和 `PLATFORM` 的设置，可以决定当前编译的是 `wechat` or `alipay` 的 `sandbox` or `production`
&nbsp;
