#### 环境安装

请先全局安装  aixy

```
npm install  aixy -g

安装完毕后请在运行以下命令，确认一下是否安装成功
aixy -V
```

确认 aixy 安装成功后，请运行项目根目录下的 init 脚本 

```
./init
```

某些情况下 init 脚本运行可能会因为本地 npm 设置或者网络等问题部分运行不成功，运行完毕 init 脚本后请检查一下 vue、vue-router 是否安装好了，项目根目录中 server 目录下的 npm install 是否成功运行，尽量使用淘宝源

#### 启动(默认 H5 History 模式)
一切都安装好之后，让我们继续往下

目前我在项目中仅定义了 3 种皮肤，分别为：橙色(默认)、粉色、蓝色，可以分别运行以下命令(分别运行哦)，然后会在项目根目录下的 public 目录下发现 3个目录，这3个目录就代表了 3 种不同的皮肤：
```
默认颜色
aixy dev

编译粉色
aixy dev --theme  pink

编译蓝色皮肤
aixy dev --theme  blue
```

因目前只是 demo 展示，我将 koa server 的端口设为了 3001(可自行在 server/config.js 中配置端口)，在浏览器输入以下地址查看，目前路由使用的是 H5 History 模式：

```
http://localhost:3001
```

此时应该是橙色皮肤，考虑到皮肤设置值一般都是服务端接口给回的(不同的公司实现不一样)，我在这里直接通过 server/config.js 中的 theme 字段来控制皮肤值，可尝试更改它然后重新运行 aixy 命令(或者直接运行 node server/server.js)，刷新浏览器，就能看到皮肤发生了变化

###  hash 模式

如果你想切换到 hash 模式，可将 src/router/index.js 中的 mode: 'history' 注释掉，然后去 server/server.js 中启用 filters/handle-dir.js 这个中间件，然后当然，所有皮肤都要重新编译，然后浏览器输入下面的地址：

```
http://localhost:3001
```

你最终会看到浏览器的地址变成了：

```
http://localhost:3001/mobile/1.0.0/#/index
```

然后，继续更改 server/config.js 中的 theme 值为 pink、blue，重新启动 server，浏览器地址输入 **http://localhost:3001** 就能分别看到 蓝色和粉色皮肤了：

```
http://localhost:3001/theme_blue/1.0.0/#/index

http://localhost:3001/theme_pink/1.0.0/#/index
```

#### notes
以上 aixy + server 实现多皮肤的方式，仅仅是案例展示，具体的实现可根据情况不同具体处理，这里不再多说