// let set = new Set();
// set.add(NaN);
// set.add(NaN);
// set.add(null);
// set.add(undefined);
// set.add(null);
// set.add(undefined);
// set.add(true);
// set.add(true);
// set.add(false);
// set.add(false);
// set.add(+0);
// set.add(-0);
// set.add(Infinity);
// set.add(Infinity);
// console.log(set);
// console.log(set.size);
// set.delete(NaN);
// console.log(set.size);
// console.log(set.has(Infinity));

// // console.log(set.keys().forEach(ele => {
// //     console.log(ele);
// // }));
// for(let item of set.entries()){
//     console.log(item);
// }

let m = new Map([['a',  111], ['b', 2233]]);
// console.log(m);
m.set({}, 'hello');
// console.log(m.get('a'));

// console.log(m.has('a'));
for(let item of m.keys()){
    console.log(item);
}
for(let item of m.values()){
    console.log(item);
}
for(let item of m.entries()){
    console.log(item);
}

console.log('forEach:');
m.forEach((val, key) => {
    console.log(val);
    console.log(key);
});