// 购物车模块
export default {
    namespaced: true,
    state() {
        return {
            data: [],  //有一些响应式问题，要搞明白
            total: 0,
            curPage: 0
        }
    },
    mutations: {
        setCart(state, { data,total ,curPage}){
            //设置购物车信息
            state.data = data;
            state.total = total;
            state.curPage = curPage;
        }
    }
}