import Vue from 'vue';

function testHandler(el,{value}){
    el.setAttribute('data-test', value)
}
// 自定义指令
Vue.directive('test', {
    inserted: function(el, bingding){
        testHandler(el, bingding);
    },
    update: function(el, bingding){
        testHandler(el, bingding);
    }
});