// 创建路由
import VueRouter from 'vue-router';
import  routes from './routes';

var router = new VueRouter({
	mode: 'history',
	routes,
	scrollBehavior: function(to, from ,savedPosition){
		// 返回希望滚动到的位置
		console.log('这个啥时候触发？');
		console.log(to);
		console.log(savedPosition);
		// if(savedPosition){
		// 	return savedPosition;
		// }else{
		// 	return {x: 0,  y: 0};
		// }
		return {x: 0,  y: 0};
		// return pos;
	}
});
router.beforeEach(function(to, from , next){
	console.log('before all');
	console.log(to);
	// 需要校验，且无用户数据
	// 如归 state 还没有，则证明为首次进来，除非全局，目前不需要这样做
	if(to.meta.requiresAuth && router.app.$store && router.app.$store.state && router.app.$store.state.user.user && !router.app.$store.state.user.user.id){
		//设置  redirectUrl，方便登陆回退
		console.log('需要去登陆 fullPath: ' + to.fullPath);
		console.log(to);
		router.app.$store.commit('setRedirectUrl', to);
		next('/login');
	}else{
		next();
	}
});

router.beforeResolve(function(to, from , next){
	console.log('before resolve');
	next();
})

router.afterEach(function(to, from , next){
	console.log('after all');
});

// 没生效
router.onError(function(e){
    console.log('导航出错：');
	console.log(e);
});

router.onReady(function(){
	console.log('router.onReady 的执行时机');
});

export default router;