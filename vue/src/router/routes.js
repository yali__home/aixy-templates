import store from '../store';
let ROUTE_MAP = __ROUTER_MAP__;
// 处理 ROUTE_MAP

var buildMap = function(rules) {
    var ret = [];
    for(var i in rules) {
        if (rules.hasOwnProperty(i)) {
            var rule = rules[i];
            let subs = rule.subRoutes;
            if(subs && Object.keys(subs).length){
                Object.keys(subs).forEach((key)=>{
                    let ele = subs[key];
                    ret.push({
                        path: key,
                        name: ele.name,
                        component: ele.file && (function(ele){
                            return function(resolve) {
                                import('@/pages' + ele.file).then(re => {
                                    resolve(re.default) 
                                })
                            }
                        })(ele)
                    });
                });
            }
        }
    }
    console.log(ret);
    return ret
}
let maps = buildMap(ROUTE_MAP);

// 去除 hash，是
function removeHash(to){
    console.log('removeHash');
    if(to.hash) return {
        path: to.path,
        query: to.query,
        hash: ''
    }
}

function removeQueryParams(to, from, next){
    console.log('removeQueryParams:');
    console.log(to);
    console.log('我想知道能不能访问 store 对象：');
    console.log(store);
    if(Object.keys(to.query).length){
        console.log('有 query');
        next({ path: to.path, query: {}, hash: to.hash });
    }else{
        next();
    }
}

// 检测 登陆状态：要么 metadata，在  beforeEach 中处理，要么每个 route 那里，单独处理，个人倾向 beforeEach，像 aixy 这种，自动读取路由配置的，你只能说为路由记录指定一些
function checkLogin(to, from, next){}

maps = [
    {
        path: '/',
        alias: '/index',
        name: 'index',
        component: ()=>import('../pages/index/index.vue')
    },
    {
        path: '/list/:id',
        name: 'list',
        // 关于 beforeEnter，去其它页面报错了
        // 这玩意能不能不报错啊
        beforeEnter: removeQueryParams,
        // function(to, from, next){
        //     console.log('list 的 beforeEnter');
        //     // 此处不调用 next 行不行？
        //     // next(false);
        //     return false;
        //     // 为什么跳出去其它地方会报错？
        // }
        component: ()=>import('../pages/list/index.vue')
    },
    {
        path: '/form',
        name: 'form',
        meta: { requiresAuth: true },
        component: ()=>import('../pages/form/index.vue')
    },
    {
        path: '/cart',
        name: 'cart',
        component: ()=>import('../pages/cart/index.vue')
    },
    {
        path: '/user',
        name: 'user',
        meta: { requiresAuth: true },
        component: ()=>import('../pages/user/index.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: ()=>import('../pages/login/index.vue')
    },
    {
        path: '/chart',
        name: 'chart',
        component: ()=>import('../pages/chart/index.vue')
    },
    {
        path: '/cates',
        name: 'cates',
        component: ()=>import('../pages/cates/index.vue'),
        children: [
            {
                path: ':id',   //这里不要带斜杠
                component: ()=>import('../pages/cates/detail/index.vue')
            }
        ]
    },
    {
        path: '*',
        name: '404',
        component: ()=>import('../pages/404/index.vue')
    }
]

export default maps;