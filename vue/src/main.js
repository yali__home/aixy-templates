import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router';
import store from './store';
import vform from 'aixy-vue2-form';
import list from 'aixy-vue2-list';
import toast from 'aixy-vue2-toast';
import popup from 'aixy-vue2-popup';
import loading from 'aixy-vue2-loading';
import App from './App.vue';
import { copy, deepClone, type } from 'aixy-util';
import bus from 'aixy-vue2-bus';
import './core/directive';

// 入口页面，各种插件使用的地方：vue-router、vuex
// rem
Vue.use(VueRouter);

// 注册全局组件
Vue.component('vform', vform);
Vue.component('list', list);
Vue.component('toast', toast);
Vue.component('popup', popup);
Vue.component('loading', loading);

// 全局事件总线
Vue.prototype.$bus = bus;

// 挂载到 #app
let app = new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: {
		App
	}
});

// 测试深拷贝:
let date = new Date(2423423423233423);
let reg = new RegExp('^/api', 'gm');
let err = new Error('wrong');
let arr = [];
for(let i=0;i<100000;i++){
	arr.push(i);
}
let target = {
	arr,
	name: 'helo',
	age: 31,
	sex: true,
	id: Symbol(),
	say: function(){},
	nn: NaN,
	ull: null,
	und: undefined,
	sum: 0,
	ep: '',
	list: [{ id: 1},'dfdf', 'ssdsdd', 'sffs'],
	user: {
		nme: 'katy',
		age: 31
	},
	date,
	reg,
	err,
	inf: Infinity
};

// let dest = JSON.parse(JSON.stringify(target));
console.log('开始拷贝：');
console.time();
let dest = deepClone(target);
console.log('结束拷贝：');
console.timeEnd();
console.log(target);
console.log(JSON.parse(JSON.stringify(target)));
console.log(target.list[0]===dest.list[0]);
console.log(target.user===dest.user);
console.log(target.say===dest.say);
console.log(dest.id);
console.log(dest);
console.log('reg:'+type(reg));
console.log('date:'+type(date));
console.log('err:'+type(err));
