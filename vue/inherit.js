// 原型链继承
function Parent(){
    this.colors = [];
}
Parent.prototype.say = function(){
    console.log('hello');
}
// function inheritPrototype(sub,parent){
//     let pro = Object.create(parent.prototype);
//     pro.constructor = sub;
//     sub.prototype = pro;
// }
function Child(){
    // 借用构造函数继承
    Parent.call(this);
}

function A(){}
function B(){}

Child.prototype = Object.create(Parent.prototype);
Object.assign(Child.prototype, A.prototype, B.prototype);

// inheritPrototype(Child, Parent);
// // 原型链继承
// Child.prototype = new Parent();
// 原型式继承
// Child.prototype = Object.create(Parent.prototype)

// Child.prototype.test = function(){
//     console.log('test');
// }
Child.prototype.constructor = Child;

let ins = new Child();
let ins1 = new Child();
console.log(ins instanceof Child);
console.log(ins instanceof Parent);
console.log(ins instanceof A);
console.log(ins instanceof B);
console.log(ins.constructor);
ins.colors.push('blue');
ins1.colors.push('red');
console.log(ins.colors);
console.log(ins1.colors);

//原型式继承

// 寄生组合继承实现的核心代码
function _inherits(sub, parent){
    // 添加了 constructor
    sub.prototype = Object.create(parent && parent.prototype, {
        constructor: {
            valye: sub,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    if(parent){
        Object.setPrototypeOf?Object.setPrototypeOf(sub, parent):sub.__proto__ = sub;
    }
}
