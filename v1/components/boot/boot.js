import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from '../routes';

Vue.use(VueRouter);

var router = new VueRouter({
	hashbang: false
});

router.map(routes);
console.log(routes);
// 挂载到 #app
router.start(Vue.extend({}), '#app')
