<div class="P-index">
    <div class="t">欢迎来到 aixy 微信小程序案例展示</div>

    <div class="btn">
        <button bindtap="showTip">请点我弹出 toast 组件</button>
    </div>
    <div class="btn theme">
        <button bindtap="toggleTheme" data-type="pink">粉色皮肤</button>
        <button bindtap="toggleTheme" data-type="blue">蓝色皮肤</button>
    </div>

    <div class="tip">本页面是没有配置 json 文件的，json 文件是 aixy 自动生成的，组件引用路径，也是 aixy 自动生成的。可以看一下 pages/index 的源码哦</div>

    <div class="t">
        <div class="cell">本地根目录 images 下图片</div>
        <div class="cell">远程图片(微信小程序开发工具查看 css)</div>
    </div>

    <div class="pic">
        <image src="/images/smile.jpg" width="100rpx" height="100rpx"></image>
        <div class="cell"></div>
    </div>

    <toast wx:if="{{show}}" text="{{tip}}"></toast>

</div>