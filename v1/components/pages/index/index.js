const Vue = require('vue');
const toast = require('components/toast');
module.exports = Vue.extend({
    template: require('./index.tpl'),
    data: function(){
        return {
            showToast: false,
            toastTip: ''
        }
    },
    components: {
        toast
    }
});