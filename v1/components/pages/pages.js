const Vue = require('Vue');
const VueRouter = require('vue-router');
Vue.use(VueRouter);

module.exports = Vue.extend({
    template: __inline('./pages.tpl'),
    data: function(){
        return {
            msg: 'Hello'
        }
    }
})