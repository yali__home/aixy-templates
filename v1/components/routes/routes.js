let ROUTE_MAP = __ROUTE_MAP__;
// 处理 ROUTE_MAP

var buildMap = function(rules) {
    var ret = {}
    for(var i in rules) {
        if (rules.hasOwnProperty(i)) {
            var rule = rules[i];
            if (!rule.file) continue
            ret[i] = {
                name: rule.name,
                component: rule.file && (function(rule){
                    return function(resolve) {
                        import('@/pages' + rule.file).then(re => {
                            resolve(re.default) 
                        })
                    }
                })(rule)
            }
            if (rule.subRoutes) {
                ret[i].subRoutes = buildMap(rule.subRoutes)
            }
        }
    }
    return ret
}
let maps = buildMap(ROUTE_MAP);

export default maps;