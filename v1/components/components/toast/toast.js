const Vue = require('vue');
module.exports = Vue.extend({
    template: require('./toast.tpl'),
    props: {
        show: {
            type: Boolean,
            default: false
        },
        show: {
            type: String,
            default: ''
        }
    },
    methods: {
        toggleStatus() {
            this.show = !this.show;
        },
        fade() {
            clearTimeout(this.timer);
            this.timer = setTimeout(() => {
                this.toggleStatus();
                this.timer = null;
            }, 2000);
        }
    },
    created: function () {
        this.timer = null;
    },
    mounted: function () {
        if (this.show) {
            this.fade();
        }
    },
    watch: {
        show: function (nval, oval) {
            if (nval && nval != oval) {
                clearTimeout(this.timer);
                this.timer = setTimeout(() => {
                    this.toggleStatus();
                    this.timer = null;
                }, 2000);
            }
        }
    }
});