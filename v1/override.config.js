let  theme = require('./theme');
let { version } = require('./package.json');
let COLOR = process.env.THEME_COLOR;
const child_process = require('child_process');

module.exports.profile = 'v1';
module.exports.init = function(Config){
    return class MyConfig extends Config{
         constructor(){
             super();
            //  默认的皮肤配置为蓝色
             this.config.styleLoaderOptions = {
                 less: {
                     lessOptions: {
                         modifyVars: theme['blue']
                     }
                 }
             }
             
            //  如果系统变量中有 THEME_COLOR 值，多皮肤目录随着产出
            if(COLOR && theme[COLOR]){
                this.config.assertPath = `theme_${COLOR}/${version}`;
                this.config.styleLoaderOptions.less.lessOptions.modifyVars = theme[COLOR];
            }
         }
         onDone(){
            //  编译成功后，启动 server/server.js，暂时不需要，工具内部启动了 server
            // child_process.spawn('node', ['./server/server.js'], {
            //     env: process.env,
            //     stdio: 'inherit'
            // }).on('exit', code=>{
            //     process.exit(code);
            // });
            // console.log('全局配置：');
            // console.log(this.config);
         }
         onConfig(config){
         }
    }
}